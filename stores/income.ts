import {defineStore} from 'pinia';

// interface IIncome {
//   delivery: number
//   feeRider: number
//   feeAdmin: number
//   feeTotal: number
//   taxSan: number
//   taxGo: number
//   dateIncome: string
//   bossName: string
//   timeIncome: string
// }

export const useIncomeStore = defineStore('income',{
  state:() => ({
    list: [],
    staticsData: [],
  }),
  actions: {
    async fetchIncome() {
      const token = useCookie('token');
      const {data}: any = await useFetch(
        `http://matddak.shop:8080/v1/income/all/1`, {
            method: 'GET',
            headers: {
              'Authorization': `Bearer ${token}`
            },
        });
      if (data.value) {
        this.list = data.value.list;
      }
    },
    async fetchStatics() {
      const token = useCookie('token');
      const {data}: any = await useFetch(
        `http://matddak.shop:8080/v1/income/statistics/admin`, {
          method: 'GET',
          headers: {
            'Authorization': `Bearer ${token}`
          },
        });
      if (data.value) {
        this.staticsData = data.value.data;
      }
    },
  }
})
if (import.meta.hot) {  //HMR
  import.meta.hot.accept(acceptHMRUpdate(useIncomeStore, import.meta.hot))
}
