import {defineStore} from 'pinia';

interface IShopCreate {
    storeName: string
    addressStoreDo: string
    addressStoreG: string
    storeNumber: string
    sellTime: string
    holiday: string
    deliveryArea: string
    bossName: string
    shopId: string
    storeX: number
    storeY: number
}

interface IStore {
    storeName: string
    addressStoreDo: string
    addressStoreG: string
    storeNumber: string
    sellTime: string
    holiday: string
    deliveryArea: string
    bossName: string
    shopId: string
    storeX: number
    storeY: number
}


// auth 스토어
export const useShopStore = defineStore('shop', {
    state: () => ({
        list: [],
    }),
    actions: {
        setShop(data: IShopCreate) {
            useFetch('http://matddak.shop:8080/v1/store/new', {
                method: 'POST',
                body: data,
            })
        },
        async fetchShop() {
            const token = useCookie('token');
            const {data}: any = await useFetch(
                'http://matddak.shop:8080/v1/store/all', {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${token}`
                    },
                }
            );
            if (data.value) {
                this.list = data.value.list;
                console.log(data);
            }
        },
      async deleteShop(id: string) {
        const token = useCookie('token');
        // const {id} = useRoute().params
        const {data}: any = await useFetch(
          `http://matddak.shop:8080/v1/store/delete/${id}`, {
            method: 'DELETE',
            headers: {
              'Authorization': `Bearer ${token}`
            },
          },
        );
        if (data.value) {
          this.list = data.value.data;
        }
      }

    }
})

if (import.meta.hot) {  //HMR
    import.meta.hot.accept(acceptHMRUpdate(useShopStore, import.meta.hot))
}
