import {defineStore} from 'pinia';


// board 스토어
export const useIndexStore = defineStore('indexList', {
    state: () => ({
        dash: [],
    }),
    actions: {
        async fetchIndex() {
            const token = useCookie('token');
            const {data}: any = await useFetch(
                `http://matddak.shop:8080/v1/dash/board`, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${token}`
                    },
                },
            );
            if (data.value) {
                this.dash = data.value.data.boardItems;
                console.log(data.value.data.boardItems)
            }
        },
        async fetchRiderIndex() {
            const token = useCookie('token');
            const {data}: any = await useFetch(
                `http://matddak.shop:8080/v1/dash/board`, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${token}`
                    },
                },
            );
            if (data.value) {
                this.dash = data.value.data.riderItems;
                console.log(data.value.data.riderItems)
            }
        }
    }
})

if (import.meta.hot) {  //HMR
    import.meta.hot.accept(acceptHMRUpdate(useIndexStore, import.meta.hot))
}
