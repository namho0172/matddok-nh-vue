import {defineStore} from 'pinia';
import type {INotice} from "~/interface";


// board 스토어
export const useNoticeStore = defineStore('boardList', {
    state: () => ({
        boards: [],
    }),
    actions: {
        setBoard(data: INotice) {
            useFetch('http://matddak.shop:8080/v1/board/new', {
                method: 'POST',
                body: data
            })
        },
        async fetchNoticeList() {
            const token = useCookie('token');
            const {data}: any = await useFetch(
                `http://matddak.shop:8080/v1/board/all`, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${token}`
                    },
                },
            );
            if (data.value) {
                this.boards = data.value.list;
            }
        },

        async fetchNotice(boardId: string) {
            const token = useCookie('token');
            const {id} = useRoute().params;
            const {data}: any = await useFetch(
                `http://matddak.shop:8080/v1/board/detail/${id}`, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${token}`
                    },
                },
            );
            if (data.value) {
                this.boards = data.value.data;
                console.log(data.value.data.id)
            }
        },
        async delNotice(boardId: number) {
            const token = useCookie('token');
            const {id} = useRoute().params
            const {data}: any = await useFetch(
                `http://matddak.shop:8080/v1/board/delete/${id}`, {
                    method: 'DELETE',
                    headers: {
                        'Authorization': `Bearer ${token}`
                    },
                },
            );
            if (data.value) {
                this.boards = data.value.data;
            }
        }
    }
})

if (import.meta.hot) {  //HMR
    import.meta.hot.accept(acceptHMRUpdate(useNoticeStore, import.meta.hot))
}
