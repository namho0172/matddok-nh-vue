export interface UserPayloadInterface {
    email: string;
    password: string;
}

export interface INotice {
    title: string
    text: string
    // img: string
}

export interface IRiderPage {
  totalCount: number
  totalPage: number
  currentPage: number
}
export interface IndexBoard {
    id: number
    title: string
    dateCreate: string
    name: string
    dateJoin: string
    phoneNumber: string
    driveType: string
    addressWish: string
}
